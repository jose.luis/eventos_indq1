import 'package:eventos_indq/pages/register.dart';
import 'package:flutter/material.dart';

class Logins extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
      final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

    return Scaffold(
      appBar: AppBar(title: Text("INDQ Events" ) ),
        body: Container(
        key: _formKey,
        padding: EdgeInsets.symmetric(horizontal: 16),
        decoration: BoxDecoration(color: Color(0xFFFAFAFA)),
        child: ListView(
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top:16)),
            Text("Correo"),
            _InputEmail(),
            Text("Contraseña"),
            _InputPassword(),
            _CreateButtonAccess(_formKey),
            _CreateLinkRegister(context),
          ],
        ),
      ),
    );
    
  }

  Widget _InputEmail(){
    return  Padding(
      padding: EdgeInsets.only(top: 5,left:25 ,bottom: 20),
    child: Container(
      child: TextFormField(
        decoration: InputDecoration(hintText: "Ingrese tu correo electronico"),
           validator:(String value){
            if (value.isEmpty) {
              return "Necesita llenar el campo";
              print("email");
            }
          },

      ),
    ),
 );
  }
 Widget _InputPassword(){
      return  Padding(
      padding: EdgeInsets.only(top: 5,left:25 ,bottom: 20),
    child: Container(
      child: TextFormField(
        decoration: InputDecoration(hintText: "Ingrese su contraseña"),
        validator: (String value){
          if (value.isEmpty){
            return "Necesita llenar los campos";
             print("passwrod");
          }
        },
      ),
    ),
 );

    
  }
   Widget _CreateButtonAccess(_formKey){
  return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(height: 30),
          RaisedButton(
            textColor: Colors.white,
            padding: const EdgeInsets.all(0.0),
            child: Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(0xFF0D47A1),
                    Color(0xFF1976D2),
                    Color(0xFF42A5F5),
                  ],
                ),
              ),
              padding: const EdgeInsets.all(10.0),
              child: const Text( 'Acceder',
                style: TextStyle(fontSize: 20)
              ),
            ),
            onPressed: () {
               if (!_formKey.currentState.validate()){
              return"necesita llenar el campo"; 

            }
              _formKey.currentState.save();
            },
          ),
        ],
      ),
    );


  }

  Widget _CreateLinkRegister(BuildContext context ){
    final route = MaterialPageRoute(
      builder:(context){
        return registerpages();
      }
     );

    return Container(
      
       padding: EdgeInsets.only(top: 5,left:25 ,bottom: 20),
       child:InkWell(
         child: Text("registrate con nosotros ",style: TextStyle(fontSize: 20)),
         onTap:(){
            print("asassas");
            Navigator.push(context, route);
         },
    ),
    );
  }



}