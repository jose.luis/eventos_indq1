import 'package:eventos_indq/pages/register_event.dart';
import 'package:flutter/material.dart';


class View_Event extends StatefulWidget { 
  @override
  _View_EventState createState() => _View_EventState();
}

class _View_EventState extends State<View_Event> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista de eventos"),

      ),
      body: Text("hoa,mundo"),
      floatingActionButton: _CreateButton(context),
      
      
    );
  }

  Widget _CreateButton(BuildContext context){
    final route = MaterialPageRoute(
      builder:(context){
        return Register_event();
      });

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        FloatingActionButton(
          child: Icon(Icons.add),
          onPressed:(){
            Navigator.push(context, route);
          
          }    
        ),
      ],
    );


  }
}