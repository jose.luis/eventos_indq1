

import 'package:eventos_indq/pages/login.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class registerpages  extends StatefulWidget {

  @override
  _registerpagesState createState() => _registerpagesState();
}

class _registerpagesState extends State<registerpages> {
final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  String parametro;
  String _name;
  String _email;
  String _password;
  String gender;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Registro"),),
          body: Form(
            key: formKey,
        child: ListView(
          padding: EdgeInsets.only(left:20),
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top:16)),
             Text("Nombres(s)"),
            //_CreateName(),
            _CreateElement("ingrese su nombre"),
            Text("Apellido (s)"),
            _CreateElement("ingrese su apellido"),
            Text("Correo"),
            _CreateEmail(),
            Text("Contraseña"),
            _CreateELemenPassword("Ingrese su contrasena"),
            Text("Confirmar Contraseña"),
            _CreateELemenPassword("Confirme si contrasena"),
            _CreateButtonRegister(formKey),
             _CreateLinkLogin(context)

          ],
          
        ),
        
      ),  
    );
  }

   Widget _CreateElement(parametro){
     
    return  Padding(
      padding: EdgeInsets.only(top: 5,left:25 ,bottom: 15),
      child: Container(
        child: TextFormField(
          decoration: InputDecoration(hintText: parametro),
          textCapitalization: TextCapitalization.sentences  ,
          validator: (String value){
            if(value.isEmpty){
              return "Necesita llenar este campo**";
            }
          },
          onSaved: (String value){
            if (parametro=="ingrese su nombre"){
                print("ingreso siu nombre");
            }else{
              print("ingreso su segundo nombre");

            }

          },
        ),
        
      ),
    );

  }

Widget _CreateELemenPassword(parametro){
return  Padding(
      padding: EdgeInsets.only(top: 5,left:25 ,bottom: 15),
      child: Container(
        child: TextFormField(
          decoration: InputDecoration(hintText: parametro),
          obscureText: true,
          validator: (String value){
          
            if(value.isEmpty){
              return "Necesita llenar este campo**";
            }
            if (value.length <= 8){
                return"La contraseña debe tener un mínimo de 8 caracteres";
            }
          },
          onSaved: (String value){


          },
        ),
      ),
    );
}


Widget _CreateEmail(){

return  Padding(
      padding: EdgeInsets.only(top: 5,left:25 ,bottom: 15),
      child: Container(
        child: TextFormField(
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(hintText: "Ingrese Su Email"),
          validator: (String value){
            if(value.isEmpty){
              return "Necesita llenar este campo**";
            }
            if(!RegExp(r'^.+@[a-zA-Z]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)$').hasMatch(value)){
              return "El correo electrónico ingresado es incorrecto ";
            }
          },
        ),
      ),
 );
}

Widget _CreateButtonRegister(formKey){

 return Center(
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        RaisedButton(

          textColor: Colors.white,
          padding: const EdgeInsets.all(0.0),
          child: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: <Color>[
                  Color(0xFF0D47A1),
                  Color(0xFF1976D2),
                  Color(0xFF42A5F5),
                ],
              ),
            ),
              padding: const EdgeInsets.all(10.0),
            child: const Text(
              'Registrar',
              style: TextStyle(fontSize: 25)
            ),
            
          ),
          onPressed: () {
            if (!formKey.currentState.validate()){
              return"necesita llenar el campo"; 

            }
              formKey.currentState.save();
          },
        ),
      ],
    ),
  );
 

}

  Widget _CreateLinkLogin(BuildContext context ){
    final route = MaterialPageRoute(
      builder:(context){
        return Logins();
      }
       );
    return Center(
      child: Container(
         padding: EdgeInsets.only(top: 30,left:5 ,bottom: 20),
         child:InkWell( 
            child: Text("Ya eres mienbro? Inicia sesion ",style: TextStyle(fontSize: 15)),
           onTap:(){
              print("Entrando a regitrar");
              Navigator.pop(context);
           },
      ),
      ),
    );
  }

  }