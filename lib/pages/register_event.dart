

import 'package:flutter/material.dart';


class Register_event extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Agrega un evento"),
      ),
      body: Form(
        child:Row(
          children: <Widget>[
           Text("Titulo del evento"),
           TextField(
             decoration:InputDecoration(hintText: "Ingrese el titulo de su evento"),
             expands:true,
             style: TextStyle(fontSize: 16) , 
           )
          ],
        ) 
      ),

  );
  }
}